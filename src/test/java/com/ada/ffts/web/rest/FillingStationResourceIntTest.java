package com.ada.ffts.web.rest;

import com.ada.ffts.FftsApp;

import com.ada.ffts.domain.FillingStation;
import com.ada.ffts.repository.FillingStationRepository;
import com.ada.ffts.service.FillingStationService;
import com.ada.ffts.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the FillingStationResource REST controller.
 *
 * @see FillingStationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FftsApp.class)
public class FillingStationResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LOCATION = "AAAAAAAAAA";
    private static final String UPDATED_LOCATION = "BBBBBBBBBB";

    private static final String DEFAULT_DISPLAY_NAME = "AAAAAAAAAA";
    private static final String UPDATED_DISPLAY_NAME = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ACTIVE = false;
    private static final Boolean UPDATED_ACTIVE = true;

    private static final Integer DEFAULT_NO_OF_GUNS = 1;
    private static final Integer UPDATED_NO_OF_GUNS = 2;

    @Autowired
    private FillingStationRepository fillingStationRepository;

    @Autowired
    private FillingStationService fillingStationService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restFillingStationMockMvc;

    private FillingStation fillingStation;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final FillingStationResource fillingStationResource = new FillingStationResource(fillingStationService);
        this.restFillingStationMockMvc = MockMvcBuilders.standaloneSetup(fillingStationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FillingStation createEntity(EntityManager em) {
        FillingStation fillingStation = new FillingStation()
            .name(DEFAULT_NAME)
            .location(DEFAULT_LOCATION)
            .displayName(DEFAULT_DISPLAY_NAME)
            .active(DEFAULT_ACTIVE)
            .noOfGuns(DEFAULT_NO_OF_GUNS);
        return fillingStation;
    }

    @Before
    public void initTest() {
        fillingStation = createEntity(em);
    }

    @Test
    @Transactional
    public void createFillingStation() throws Exception {
        int databaseSizeBeforeCreate = fillingStationRepository.findAll().size();

        // Create the FillingStation
        restFillingStationMockMvc.perform(post("/api/filling-stations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fillingStation)))
            .andExpect(status().isCreated());

        // Validate the FillingStation in the database
        List<FillingStation> fillingStationList = fillingStationRepository.findAll();
        assertThat(fillingStationList).hasSize(databaseSizeBeforeCreate + 1);
        FillingStation testFillingStation = fillingStationList.get(fillingStationList.size() - 1);
        assertThat(testFillingStation.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testFillingStation.getLocation()).isEqualTo(DEFAULT_LOCATION);
        assertThat(testFillingStation.getDisplayName()).isEqualTo(DEFAULT_DISPLAY_NAME);
        assertThat(testFillingStation.isActive()).isEqualTo(DEFAULT_ACTIVE);
        assertThat(testFillingStation.getNoOfGuns()).isEqualTo(DEFAULT_NO_OF_GUNS);
    }

    @Test
    @Transactional
    public void createFillingStationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = fillingStationRepository.findAll().size();

        // Create the FillingStation with an existing ID
        fillingStation.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFillingStationMockMvc.perform(post("/api/filling-stations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fillingStation)))
            .andExpect(status().isBadRequest());

        // Validate the FillingStation in the database
        List<FillingStation> fillingStationList = fillingStationRepository.findAll();
        assertThat(fillingStationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = fillingStationRepository.findAll().size();
        // set the field null
        fillingStation.setName(null);

        // Create the FillingStation, which fails.

        restFillingStationMockMvc.perform(post("/api/filling-stations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fillingStation)))
            .andExpect(status().isBadRequest());

        List<FillingStation> fillingStationList = fillingStationRepository.findAll();
        assertThat(fillingStationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLocationIsRequired() throws Exception {
        int databaseSizeBeforeTest = fillingStationRepository.findAll().size();
        // set the field null
        fillingStation.setLocation(null);

        // Create the FillingStation, which fails.

        restFillingStationMockMvc.perform(post("/api/filling-stations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fillingStation)))
            .andExpect(status().isBadRequest());

        List<FillingStation> fillingStationList = fillingStationRepository.findAll();
        assertThat(fillingStationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDisplayNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = fillingStationRepository.findAll().size();
        // set the field null
        fillingStation.setDisplayName(null);

        // Create the FillingStation, which fails.

        restFillingStationMockMvc.perform(post("/api/filling-stations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fillingStation)))
            .andExpect(status().isBadRequest());

        List<FillingStation> fillingStationList = fillingStationRepository.findAll();
        assertThat(fillingStationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNoOfGunsIsRequired() throws Exception {
        int databaseSizeBeforeTest = fillingStationRepository.findAll().size();
        // set the field null
        fillingStation.setNoOfGuns(null);

        // Create the FillingStation, which fails.

        restFillingStationMockMvc.perform(post("/api/filling-stations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fillingStation)))
            .andExpect(status().isBadRequest());

        List<FillingStation> fillingStationList = fillingStationRepository.findAll();
        assertThat(fillingStationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllFillingStations() throws Exception {
        // Initialize the database
        fillingStationRepository.saveAndFlush(fillingStation);

        // Get all the fillingStationList
        restFillingStationMockMvc.perform(get("/api/filling-stations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fillingStation.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].location").value(hasItem(DEFAULT_LOCATION.toString())))
            .andExpect(jsonPath("$.[*].displayName").value(hasItem(DEFAULT_DISPLAY_NAME.toString())))
            .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())))
            .andExpect(jsonPath("$.[*].noOfGuns").value(hasItem(DEFAULT_NO_OF_GUNS)));
    }

    @Test
    @Transactional
    public void getFillingStation() throws Exception {
        // Initialize the database
        fillingStationRepository.saveAndFlush(fillingStation);

        // Get the fillingStation
        restFillingStationMockMvc.perform(get("/api/filling-stations/{id}", fillingStation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(fillingStation.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.location").value(DEFAULT_LOCATION.toString()))
            .andExpect(jsonPath("$.displayName").value(DEFAULT_DISPLAY_NAME.toString()))
            .andExpect(jsonPath("$.active").value(DEFAULT_ACTIVE.booleanValue()))
            .andExpect(jsonPath("$.noOfGuns").value(DEFAULT_NO_OF_GUNS));
    }

    @Test
    @Transactional
    public void getNonExistingFillingStation() throws Exception {
        // Get the fillingStation
        restFillingStationMockMvc.perform(get("/api/filling-stations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFillingStation() throws Exception {
        // Initialize the database
        fillingStationService.save(fillingStation);

        int databaseSizeBeforeUpdate = fillingStationRepository.findAll().size();

        // Update the fillingStation
        FillingStation updatedFillingStation = fillingStationRepository.findOne(fillingStation.getId());
        updatedFillingStation
            .name(UPDATED_NAME)
            .location(UPDATED_LOCATION)
            .displayName(UPDATED_DISPLAY_NAME)
            .active(UPDATED_ACTIVE)
            .noOfGuns(UPDATED_NO_OF_GUNS);

        restFillingStationMockMvc.perform(put("/api/filling-stations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedFillingStation)))
            .andExpect(status().isOk());

        // Validate the FillingStation in the database
        List<FillingStation> fillingStationList = fillingStationRepository.findAll();
        assertThat(fillingStationList).hasSize(databaseSizeBeforeUpdate);
        FillingStation testFillingStation = fillingStationList.get(fillingStationList.size() - 1);
        assertThat(testFillingStation.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testFillingStation.getLocation()).isEqualTo(UPDATED_LOCATION);
        assertThat(testFillingStation.getDisplayName()).isEqualTo(UPDATED_DISPLAY_NAME);
        assertThat(testFillingStation.isActive()).isEqualTo(UPDATED_ACTIVE);
        assertThat(testFillingStation.getNoOfGuns()).isEqualTo(UPDATED_NO_OF_GUNS);
    }

    @Test
    @Transactional
    public void updateNonExistingFillingStation() throws Exception {
        int databaseSizeBeforeUpdate = fillingStationRepository.findAll().size();

        // Create the FillingStation

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restFillingStationMockMvc.perform(put("/api/filling-stations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fillingStation)))
            .andExpect(status().isCreated());

        // Validate the FillingStation in the database
        List<FillingStation> fillingStationList = fillingStationRepository.findAll();
        assertThat(fillingStationList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteFillingStation() throws Exception {
        // Initialize the database
        fillingStationService.save(fillingStation);

        int databaseSizeBeforeDelete = fillingStationRepository.findAll().size();

        // Get the fillingStation
        restFillingStationMockMvc.perform(delete("/api/filling-stations/{id}", fillingStation.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<FillingStation> fillingStationList = fillingStationRepository.findAll();
        assertThat(fillingStationList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FillingStation.class);
        FillingStation fillingStation1 = new FillingStation();
        fillingStation1.setId(1L);
        FillingStation fillingStation2 = new FillingStation();
        fillingStation2.setId(fillingStation1.getId());
        assertThat(fillingStation1).isEqualTo(fillingStation2);
        fillingStation2.setId(2L);
        assertThat(fillingStation1).isNotEqualTo(fillingStation2);
        fillingStation1.setId(null);
        assertThat(fillingStation1).isNotEqualTo(fillingStation2);
    }
}
