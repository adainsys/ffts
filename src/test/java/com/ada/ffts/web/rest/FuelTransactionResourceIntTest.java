package com.ada.ffts.web.rest;

import com.ada.ffts.FftsApp;

import com.ada.ffts.domain.FuelTransaction;
import com.ada.ffts.repository.FuelTransactionRepository;
import com.ada.ffts.service.FuelTransactionService;
import com.ada.ffts.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import static com.ada.ffts.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the FuelTransactionResource REST controller.
 *
 * @see FuelTransactionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FftsApp.class)
public class FuelTransactionResourceIntTest {

    private static final String DEFAULT_DRIVER_NAME = "AAAAAAAAAA";
    private static final String UPDATED_DRIVER_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_VEHICLE_NO = "AAAAAAAAAA";
    private static final String UPDATED_VEHICLE_NO = "BBBBBBBBBB";

    private static final String DEFAULT_OPENING_KM = "AAAAAAAAAA";
    private static final String UPDATED_OPENING_KM = "BBBBBBBBBB";

    private static final String DEFAULT_CLOSING_KM = "AAAAAAAAAA";
    private static final String UPDATED_CLOSING_KM = "BBBBBBBBBB";

    private static final String DEFAULT_RKM = "AAAAAAAAAA";
    private static final String UPDATED_RKM = "BBBBBBBBBB";

    private static final String DEFAULT_HSD = "AAAAAAAAAA";
    private static final String UPDATED_HSD = "BBBBBBBBBB";

    private static final String DEFAULT_BAV = "AAAAAAAAAA";
    private static final String UPDATED_BAV = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_DATE_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_SUPERVISOR_NAME = "AAAAAAAAAA";
    private static final String UPDATED_SUPERVISOR_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_VEHICLE_COMPANY = "AAAAAAAAAA";
    private static final String UPDATED_VEHICLE_COMPANY = "BBBBBBBBBB";

    private static final String DEFAULT_VEHICLE_COMING_FROM = "AAAAAAAAAA";
    private static final String UPDATED_VEHICLE_COMING_FROM = "BBBBBBBBBB";

    private static final String DEFAULT_VEHICLE_WENT_TO = "AAAAAAAAAA";
    private static final String UPDATED_VEHICLE_WENT_TO = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE_NO = "AAAAAAAAAA";
    private static final String UPDATED_PHONE_NO = "BBBBBBBBBB";

    private static final String DEFAULT_HSD_INWORDS = "AAAAAAAAAA";
    private static final String UPDATED_HSD_INWORDS = "BBBBBBBBBB";

    private static final String DEFAULT_RECEIPT_NO = "AAAAAAAAAA";
    private static final String UPDATED_RECEIPT_NO = "BBBBBBBBBB";

    @Autowired
    private FuelTransactionRepository fuelTransactionRepository;

    @Autowired
    private FuelTransactionService fuelTransactionService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restFuelTransactionMockMvc;

    private FuelTransaction fuelTransaction;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final FuelTransactionResource fuelTransactionResource = new FuelTransactionResource(fuelTransactionService);
        this.restFuelTransactionMockMvc = MockMvcBuilders.standaloneSetup(fuelTransactionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FuelTransaction createEntity(EntityManager em) {
        FuelTransaction fuelTransaction = new FuelTransaction()
            .driverName(DEFAULT_DRIVER_NAME)
            .vehicleNo(DEFAULT_VEHICLE_NO)
            .openingKm(DEFAULT_OPENING_KM)
            .closingKm(DEFAULT_CLOSING_KM)
            .rkm(DEFAULT_RKM)
            .hsd(DEFAULT_HSD)
            .bav(DEFAULT_BAV)
            .dateTime(new Date())
            .supervisorName(DEFAULT_SUPERVISOR_NAME)
            .vehicleCompany(DEFAULT_VEHICLE_COMPANY)
            .vehicleComingFrom(DEFAULT_VEHICLE_COMING_FROM)
            .vehicleWentTo(DEFAULT_VEHICLE_WENT_TO)
            .phoneNo(DEFAULT_PHONE_NO)
            .hsdInwords(DEFAULT_HSD_INWORDS)
            .receiptNo(DEFAULT_RECEIPT_NO);
        return fuelTransaction;
    }

    @Before
    public void initTest() {
        fuelTransaction = createEntity(em);
    }

    @Test
    @Transactional
    public void createFuelTransaction() throws Exception {
        int databaseSizeBeforeCreate = fuelTransactionRepository.findAll().size();

        // Create the FuelTransaction
        restFuelTransactionMockMvc.perform(post("/api/fuel-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fuelTransaction)))
            .andExpect(status().isCreated());

        // Validate the FuelTransaction in the database
        List<FuelTransaction> fuelTransactionList = fuelTransactionRepository.findAll();
        assertThat(fuelTransactionList).hasSize(databaseSizeBeforeCreate + 1);
        FuelTransaction testFuelTransaction = fuelTransactionList.get(fuelTransactionList.size() - 1);
        assertThat(testFuelTransaction.getDriverName()).isEqualTo(DEFAULT_DRIVER_NAME);
        assertThat(testFuelTransaction.getVehicleNo()).isEqualTo(DEFAULT_VEHICLE_NO);
        assertThat(testFuelTransaction.getOpeningKm()).isEqualTo(DEFAULT_OPENING_KM);
        assertThat(testFuelTransaction.getClosingKm()).isEqualTo(DEFAULT_CLOSING_KM);
        assertThat(testFuelTransaction.getRkm()).isEqualTo(DEFAULT_RKM);
        assertThat(testFuelTransaction.getHsd()).isEqualTo(DEFAULT_HSD);
        assertThat(testFuelTransaction.getBav()).isEqualTo(DEFAULT_BAV);
        assertThat(testFuelTransaction.getDateTime()).isEqualTo(DEFAULT_DATE_TIME);
        assertThat(testFuelTransaction.getSupervisorName()).isEqualTo(DEFAULT_SUPERVISOR_NAME);
        assertThat(testFuelTransaction.getVehicleCompany()).isEqualTo(DEFAULT_VEHICLE_COMPANY);
        assertThat(testFuelTransaction.getVehicleComingFrom()).isEqualTo(DEFAULT_VEHICLE_COMING_FROM);
        assertThat(testFuelTransaction.getVehicleWentTo()).isEqualTo(DEFAULT_VEHICLE_WENT_TO);
        assertThat(testFuelTransaction.getPhoneNo()).isEqualTo(DEFAULT_PHONE_NO);
        assertThat(testFuelTransaction.getHsdInwords()).isEqualTo(DEFAULT_HSD_INWORDS);
        assertThat(testFuelTransaction.getReceiptNo()).isEqualTo(DEFAULT_RECEIPT_NO);
    }

    @Test
    @Transactional
    public void createFuelTransactionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = fuelTransactionRepository.findAll().size();

        // Create the FuelTransaction with an existing ID
        fuelTransaction.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFuelTransactionMockMvc.perform(post("/api/fuel-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fuelTransaction)))
            .andExpect(status().isBadRequest());

        // Validate the FuelTransaction in the database
        List<FuelTransaction> fuelTransactionList = fuelTransactionRepository.findAll();
        assertThat(fuelTransactionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDriverNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = fuelTransactionRepository.findAll().size();
        // set the field null
        fuelTransaction.setDriverName(null);

        // Create the FuelTransaction, which fails.

        restFuelTransactionMockMvc.perform(post("/api/fuel-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fuelTransaction)))
            .andExpect(status().isBadRequest());

        List<FuelTransaction> fuelTransactionList = fuelTransactionRepository.findAll();
        assertThat(fuelTransactionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkVehicleNoIsRequired() throws Exception {
        int databaseSizeBeforeTest = fuelTransactionRepository.findAll().size();
        // set the field null
        fuelTransaction.setVehicleNo(null);

        // Create the FuelTransaction, which fails.

        restFuelTransactionMockMvc.perform(post("/api/fuel-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fuelTransaction)))
            .andExpect(status().isBadRequest());

        List<FuelTransaction> fuelTransactionList = fuelTransactionRepository.findAll();
        assertThat(fuelTransactionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkOpeningKmIsRequired() throws Exception {
        int databaseSizeBeforeTest = fuelTransactionRepository.findAll().size();
        // set the field null
        fuelTransaction.setOpeningKm(null);

        // Create the FuelTransaction, which fails.

        restFuelTransactionMockMvc.perform(post("/api/fuel-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fuelTransaction)))
            .andExpect(status().isBadRequest());

        List<FuelTransaction> fuelTransactionList = fuelTransactionRepository.findAll();
        assertThat(fuelTransactionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkClosingKmIsRequired() throws Exception {
        int databaseSizeBeforeTest = fuelTransactionRepository.findAll().size();
        // set the field null
        fuelTransaction.setClosingKm(null);

        // Create the FuelTransaction, which fails.

        restFuelTransactionMockMvc.perform(post("/api/fuel-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fuelTransaction)))
            .andExpect(status().isBadRequest());

        List<FuelTransaction> fuelTransactionList = fuelTransactionRepository.findAll();
        assertThat(fuelTransactionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSupervisorNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = fuelTransactionRepository.findAll().size();
        // set the field null
        fuelTransaction.setSupervisorName(null);

        // Create the FuelTransaction, which fails.

        restFuelTransactionMockMvc.perform(post("/api/fuel-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fuelTransaction)))
            .andExpect(status().isBadRequest());

        List<FuelTransaction> fuelTransactionList = fuelTransactionRepository.findAll();
        assertThat(fuelTransactionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkVehicleCompanyIsRequired() throws Exception {
        int databaseSizeBeforeTest = fuelTransactionRepository.findAll().size();
        // set the field null
        fuelTransaction.setVehicleCompany(null);

        // Create the FuelTransaction, which fails.

        restFuelTransactionMockMvc.perform(post("/api/fuel-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fuelTransaction)))
            .andExpect(status().isBadRequest());

        List<FuelTransaction> fuelTransactionList = fuelTransactionRepository.findAll();
        assertThat(fuelTransactionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkVehicleComingFromIsRequired() throws Exception {
        int databaseSizeBeforeTest = fuelTransactionRepository.findAll().size();
        // set the field null
        fuelTransaction.setVehicleComingFrom(null);

        // Create the FuelTransaction, which fails.

        restFuelTransactionMockMvc.perform(post("/api/fuel-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fuelTransaction)))
            .andExpect(status().isBadRequest());

        List<FuelTransaction> fuelTransactionList = fuelTransactionRepository.findAll();
        assertThat(fuelTransactionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkVehicleWentToIsRequired() throws Exception {
        int databaseSizeBeforeTest = fuelTransactionRepository.findAll().size();
        // set the field null
        fuelTransaction.setVehicleWentTo(null);

        // Create the FuelTransaction, which fails.

        restFuelTransactionMockMvc.perform(post("/api/fuel-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fuelTransaction)))
            .andExpect(status().isBadRequest());

        List<FuelTransaction> fuelTransactionList = fuelTransactionRepository.findAll();
        assertThat(fuelTransactionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPhoneNoIsRequired() throws Exception {
        int databaseSizeBeforeTest = fuelTransactionRepository.findAll().size();
        // set the field null
        fuelTransaction.setPhoneNo(null);

        // Create the FuelTransaction, which fails.

        restFuelTransactionMockMvc.perform(post("/api/fuel-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fuelTransaction)))
            .andExpect(status().isBadRequest());

        List<FuelTransaction> fuelTransactionList = fuelTransactionRepository.findAll();
        assertThat(fuelTransactionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkReceiptNoIsRequired() throws Exception {
        int databaseSizeBeforeTest = fuelTransactionRepository.findAll().size();
        // set the field null
        fuelTransaction.setReceiptNo(null);

        // Create the FuelTransaction, which fails.

        restFuelTransactionMockMvc.perform(post("/api/fuel-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fuelTransaction)))
            .andExpect(status().isBadRequest());

        List<FuelTransaction> fuelTransactionList = fuelTransactionRepository.findAll();
        assertThat(fuelTransactionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllFuelTransactions() throws Exception {
        // Initialize the database
        fuelTransactionRepository.saveAndFlush(fuelTransaction);

        // Get all the fuelTransactionList
        restFuelTransactionMockMvc.perform(get("/api/fuel-transactions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fuelTransaction.getId().intValue())))
            .andExpect(jsonPath("$.[*].driverName").value(hasItem(DEFAULT_DRIVER_NAME.toString())))
            .andExpect(jsonPath("$.[*].vehicleNo").value(hasItem(DEFAULT_VEHICLE_NO.toString())))
            .andExpect(jsonPath("$.[*].openingKm").value(hasItem(DEFAULT_OPENING_KM.toString())))
            .andExpect(jsonPath("$.[*].closingKm").value(hasItem(DEFAULT_CLOSING_KM.toString())))
            .andExpect(jsonPath("$.[*].rkm").value(hasItem(DEFAULT_RKM.toString())))
            .andExpect(jsonPath("$.[*].hsd").value(hasItem(DEFAULT_HSD.toString())))
            .andExpect(jsonPath("$.[*].bav").value(hasItem(DEFAULT_BAV.toString())))
            .andExpect(jsonPath("$.[*].dateTime").value(hasItem(sameInstant(DEFAULT_DATE_TIME))))
            .andExpect(jsonPath("$.[*].supervisorName").value(hasItem(DEFAULT_SUPERVISOR_NAME.toString())))
            .andExpect(jsonPath("$.[*].vehicleCompany").value(hasItem(DEFAULT_VEHICLE_COMPANY.toString())))
            .andExpect(jsonPath("$.[*].vehicleComingFrom").value(hasItem(DEFAULT_VEHICLE_COMING_FROM.toString())))
            .andExpect(jsonPath("$.[*].vehicleWentTo").value(hasItem(DEFAULT_VEHICLE_WENT_TO.toString())))
            .andExpect(jsonPath("$.[*].phoneNo").value(hasItem(DEFAULT_PHONE_NO.toString())))
            .andExpect(jsonPath("$.[*].hsdInwords").value(hasItem(DEFAULT_HSD_INWORDS.toString())))
            .andExpect(jsonPath("$.[*].receiptNo").value(hasItem(DEFAULT_RECEIPT_NO.toString())));
    }

    @Test
    @Transactional
    public void getFuelTransaction() throws Exception {
        // Initialize the database
        fuelTransactionRepository.saveAndFlush(fuelTransaction);

        // Get the fuelTransaction
        restFuelTransactionMockMvc.perform(get("/api/fuel-transactions/{id}", fuelTransaction.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(fuelTransaction.getId().intValue()))
            .andExpect(jsonPath("$.driverName").value(DEFAULT_DRIVER_NAME.toString()))
            .andExpect(jsonPath("$.vehicleNo").value(DEFAULT_VEHICLE_NO.toString()))
            .andExpect(jsonPath("$.openingKm").value(DEFAULT_OPENING_KM.toString()))
            .andExpect(jsonPath("$.closingKm").value(DEFAULT_CLOSING_KM.toString()))
            .andExpect(jsonPath("$.rkm").value(DEFAULT_RKM.toString()))
            .andExpect(jsonPath("$.hsd").value(DEFAULT_HSD.toString()))
            .andExpect(jsonPath("$.bav").value(DEFAULT_BAV.toString()))
            .andExpect(jsonPath("$.dateTime").value(sameInstant(DEFAULT_DATE_TIME)))
            .andExpect(jsonPath("$.supervisorName").value(DEFAULT_SUPERVISOR_NAME.toString()))
            .andExpect(jsonPath("$.vehicleCompany").value(DEFAULT_VEHICLE_COMPANY.toString()))
            .andExpect(jsonPath("$.vehicleComingFrom").value(DEFAULT_VEHICLE_COMING_FROM.toString()))
            .andExpect(jsonPath("$.vehicleWentTo").value(DEFAULT_VEHICLE_WENT_TO.toString()))
            .andExpect(jsonPath("$.phoneNo").value(DEFAULT_PHONE_NO.toString()))
            .andExpect(jsonPath("$.hsdInwords").value(DEFAULT_HSD_INWORDS.toString()))
            .andExpect(jsonPath("$.receiptNo").value(DEFAULT_RECEIPT_NO.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingFuelTransaction() throws Exception {
        // Get the fuelTransaction
        restFuelTransactionMockMvc.perform(get("/api/fuel-transactions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFuelTransaction() throws Exception {
        // Initialize the database
        fuelTransactionService.save(fuelTransaction);

        int databaseSizeBeforeUpdate = fuelTransactionRepository.findAll().size();

        // Update the fuelTransaction
        FuelTransaction updatedFuelTransaction = fuelTransactionRepository.findOne(fuelTransaction.getId());
        updatedFuelTransaction
            .driverName(UPDATED_DRIVER_NAME)
            .vehicleNo(UPDATED_VEHICLE_NO)
            .openingKm(UPDATED_OPENING_KM)
            .closingKm(UPDATED_CLOSING_KM)
            .rkm(UPDATED_RKM)
            .hsd(UPDATED_HSD)
            .bav(UPDATED_BAV)
            .dateTime(new Date())
            .supervisorName(UPDATED_SUPERVISOR_NAME)
            .vehicleCompany(UPDATED_VEHICLE_COMPANY)
            .vehicleComingFrom(UPDATED_VEHICLE_COMING_FROM)
            .vehicleWentTo(UPDATED_VEHICLE_WENT_TO)
            .phoneNo(UPDATED_PHONE_NO)
            .hsdInwords(UPDATED_HSD_INWORDS)
            .receiptNo(UPDATED_RECEIPT_NO);

        restFuelTransactionMockMvc.perform(put("/api/fuel-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedFuelTransaction)))
            .andExpect(status().isOk());

        // Validate the FuelTransaction in the database
        List<FuelTransaction> fuelTransactionList = fuelTransactionRepository.findAll();
        assertThat(fuelTransactionList).hasSize(databaseSizeBeforeUpdate);
        FuelTransaction testFuelTransaction = fuelTransactionList.get(fuelTransactionList.size() - 1);
        assertThat(testFuelTransaction.getDriverName()).isEqualTo(UPDATED_DRIVER_NAME);
        assertThat(testFuelTransaction.getVehicleNo()).isEqualTo(UPDATED_VEHICLE_NO);
        assertThat(testFuelTransaction.getOpeningKm()).isEqualTo(UPDATED_OPENING_KM);
        assertThat(testFuelTransaction.getClosingKm()).isEqualTo(UPDATED_CLOSING_KM);
        assertThat(testFuelTransaction.getRkm()).isEqualTo(UPDATED_RKM);
        assertThat(testFuelTransaction.getHsd()).isEqualTo(UPDATED_HSD);
        assertThat(testFuelTransaction.getBav()).isEqualTo(UPDATED_BAV);
        assertThat(testFuelTransaction.getDateTime()).isEqualTo(UPDATED_DATE_TIME);
        assertThat(testFuelTransaction.getSupervisorName()).isEqualTo(UPDATED_SUPERVISOR_NAME);
        assertThat(testFuelTransaction.getVehicleCompany()).isEqualTo(UPDATED_VEHICLE_COMPANY);
        assertThat(testFuelTransaction.getVehicleComingFrom()).isEqualTo(UPDATED_VEHICLE_COMING_FROM);
        assertThat(testFuelTransaction.getVehicleWentTo()).isEqualTo(UPDATED_VEHICLE_WENT_TO);
        assertThat(testFuelTransaction.getPhoneNo()).isEqualTo(UPDATED_PHONE_NO);
        assertThat(testFuelTransaction.getHsdInwords()).isEqualTo(UPDATED_HSD_INWORDS);
        assertThat(testFuelTransaction.getReceiptNo()).isEqualTo(UPDATED_RECEIPT_NO);
    }

    @Test
    @Transactional
    public void updateNonExistingFuelTransaction() throws Exception {
        int databaseSizeBeforeUpdate = fuelTransactionRepository.findAll().size();

        // Create the FuelTransaction

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restFuelTransactionMockMvc.perform(put("/api/fuel-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(fuelTransaction)))
            .andExpect(status().isCreated());

        // Validate the FuelTransaction in the database
        List<FuelTransaction> fuelTransactionList = fuelTransactionRepository.findAll();
        assertThat(fuelTransactionList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteFuelTransaction() throws Exception {
        // Initialize the database
        fuelTransactionService.save(fuelTransaction);

        int databaseSizeBeforeDelete = fuelTransactionRepository.findAll().size();

        // Get the fuelTransaction
        restFuelTransactionMockMvc.perform(delete("/api/fuel-transactions/{id}", fuelTransaction.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<FuelTransaction> fuelTransactionList = fuelTransactionRepository.findAll();
        assertThat(fuelTransactionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FuelTransaction.class);
        FuelTransaction fuelTransaction1 = new FuelTransaction();
        fuelTransaction1.setId(1L);
        FuelTransaction fuelTransaction2 = new FuelTransaction();
        fuelTransaction2.setId(fuelTransaction1.getId());
        assertThat(fuelTransaction1).isEqualTo(fuelTransaction2);
        fuelTransaction2.setId(2L);
        assertThat(fuelTransaction1).isNotEqualTo(fuelTransaction2);
        fuelTransaction1.setId(null);
        assertThat(fuelTransaction1).isNotEqualTo(fuelTransaction2);
    }
}
