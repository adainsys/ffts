(function() {
    'use strict';

    angular
        .module('fftsApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('filling-station', {
            parent: 'entity',
            url: '/filling-station?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'fftsApp.fillingStation.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/filling-station/filling-stations.html',
                    controller: 'FillingStationController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('fillingStation');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('filling-station-detail', {
            parent: 'filling-station',
            url: '/filling-station/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'fftsApp.fillingStation.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/filling-station/filling-station-detail.html',
                    controller: 'FillingStationDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('fillingStation');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'FillingStation', function($stateParams, FillingStation) {
                    return FillingStation.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'filling-station',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('filling-station-detail.edit', {
            parent: 'filling-station-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/filling-station/filling-station-dialog.html',
                    controller: 'FillingStationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['FillingStation', function(FillingStation) {
                            return FillingStation.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('filling-station.new', {
            parent: 'filling-station',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/filling-station/filling-station-dialog.html',
                    controller: 'FillingStationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                location: null,
                                displayName: null,
                                active: true,
                                noOfGuns: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('filling-station', null, { reload: 'filling-station' });
                }, function() {
                    $state.go('filling-station');
                });
            }]
        })
        .state('filling-station.edit', {
            parent: 'filling-station',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/filling-station/filling-station-dialog.html',
                    controller: 'FillingStationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['FillingStation', function(FillingStation) {
                            return FillingStation.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('filling-station', null, { reload: 'filling-station' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('filling-station.delete', {
            parent: 'filling-station',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/filling-station/filling-station-delete-dialog.html',
                    controller: 'FillingStationDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['FillingStation', function(FillingStation) {
                            return FillingStation.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('filling-station', null, { reload: 'filling-station' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
