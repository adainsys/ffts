(function() {
    'use strict';

    angular
        .module('fftsApp')
        .controller('FillingStationDetailController', FillingStationDetailController);

    FillingStationDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'FillingStation'];

    function FillingStationDetailController($scope, $rootScope, $stateParams, previousState, entity, FillingStation) {
        var vm = this;

        vm.fillingStation = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('fftsApp:fillingStationUpdate', function(event, result) {
            vm.fillingStation = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
