(function() {
    'use strict';

    angular
        .module('fftsApp')
        .controller('FillingStationDialogController', FillingStationDialogController);

    FillingStationDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'FillingStation'];

    function FillingStationDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, FillingStation) {
        var vm = this;

        vm.fillingStation = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.fillingStation.id !== null) {
                FillingStation.update(vm.fillingStation, onSaveSuccess, onSaveError);
            } else {
                FillingStation.save(vm.fillingStation, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('fftsApp:fillingStationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
