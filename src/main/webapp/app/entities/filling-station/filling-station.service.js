(function() {
    'use strict';
    angular
        .module('fftsApp')
        .factory('FillingStation', FillingStation);

    FillingStation.$inject = ['$resource'];

    function FillingStation ($resource) {
        var resourceUrl =  'api/filling-stations/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
