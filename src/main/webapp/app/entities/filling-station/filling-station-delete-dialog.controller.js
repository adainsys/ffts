(function() {
    'use strict';

    angular
        .module('fftsApp')
        .controller('FillingStationDeleteController',FillingStationDeleteController);

    FillingStationDeleteController.$inject = ['$uibModalInstance', 'entity', 'FillingStation'];

    function FillingStationDeleteController($uibModalInstance, entity, FillingStation) {
        var vm = this;

        vm.fillingStation = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            FillingStation.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
