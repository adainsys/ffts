(function() {
    'use strict';

    angular
        .module('fftsApp')
        .controller('FuelTransactionDialogController', FuelTransactionDialogController);

    FuelTransactionDialogController.$inject = ['$timeout', '$scope', '$state','$stateParams', 'entity', 'FuelTransaction', 'Vehicle','Category'];

    function FuelTransactionDialogController ($timeout, $scope,$state, $stateParams, entity, FuelTransaction, Vehicle,Category) {
        var vm = this;

        vm.fuelTransaction = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
       // vm.vehicles = Vehicle.query();
         vm.categories = Category.query();
         vm.reloadVehiclesByCategory=reloadVehiclesByCategory;


        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
           $state.go('fuel-transaction', null, { reload: false });
        }

        function save () {
            vm.isSaving = true;
         /* var data=new FormData();
            data.append('file', vm.file[0]);
            FuelTransactionFileUpload.uploadFile(data);*/

            var formData = new FormData();
             formData.append('data', new Blob([angular.toJson(vm.fuelTransaction)], {
                        type: "application/json"
                    }));

                            angular.forEach($scope.files,function(file){
                            formData.append('file',file[0]);
                            });



            if (vm.fuelTransaction.id !== null) {
                FuelTransaction.update(formData, onSaveSuccess, onSaveError);
            } else {
                FuelTransaction.save(formData, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('fftsApp:fuelTransactionUpdate', result);
            //$uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.dateTime = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }

         function reloadVehiclesByCategory(){
                vm.vehicles=Vehicle.query({type:'category',id:vm.selectedCategory.id});
          }
    }
})();
