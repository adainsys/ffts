(function() {
    'use strict';
    angular
        .module('fftsApp')
        .factory('FuelTransaction', FuelTransaction);

    FuelTransaction.$inject = ['$resource', 'DateUtils'];

    function FuelTransaction ($resource, DateUtils) {
        var resourceUrl =  'api/fuel-transactions/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.dateTime = DateUtils.convertDateTimeFromServer(data.dateTime);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' },
            'save': {
                  method: 'POST',
                  transformRequest: angular.identity,
                  headers: {
                     'Content-Type': undefined
                  }
         }

        });
    }



})();
