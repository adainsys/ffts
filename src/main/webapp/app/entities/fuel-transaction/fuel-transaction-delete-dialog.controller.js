(function() {
    'use strict';

    angular
        .module('fftsApp')
        .controller('FuelTransactionDeleteController',FuelTransactionDeleteController);

    FuelTransactionDeleteController.$inject = ['$uibModalInstance', 'entity', 'FuelTransaction'];

    function FuelTransactionDeleteController($uibModalInstance, entity, FuelTransaction) {
        var vm = this;

        vm.fuelTransaction = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            FuelTransaction.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
