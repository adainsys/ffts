(function() {
    'use strict';

    angular
        .module('fftsApp')
        .controller('FuelTransactionDetailController', FuelTransactionDetailController);

    FuelTransactionDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'FuelTransaction', 'Vehicle'];

    function FuelTransactionDetailController($scope, $rootScope, $stateParams, previousState, entity, FuelTransaction, Vehicle) {
        var vm = this;

        vm.fuelTransaction = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('fftsApp:fuelTransactionUpdate', function(event, result) {
            vm.fuelTransaction = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
