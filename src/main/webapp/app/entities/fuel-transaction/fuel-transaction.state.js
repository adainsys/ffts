(function() {
    'use strict';

    angular
        .module('fftsApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('fuel-transaction', {
            parent: 'entity',
            url: '/fuel-transaction?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'fftsApp.fuelTransaction.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/fuel-transaction/fuel-transactions.html',
                    controller: 'FuelTransactionController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('fuelTransaction');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('fuel-transaction-detail', {
            parent: 'fuel-transaction',
            url: '/fuel-transaction/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'fftsApp.fuelTransaction.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/fuel-transaction/fuel-transaction-detail.html',
                    controller: 'FuelTransactionDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('fuelTransaction');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'FuelTransaction', function($stateParams, FuelTransaction) {
                    return FuelTransaction.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'fuel-transaction',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('fuel-transaction-detail.edit', {
            parent: 'fuel-transaction-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/fuel-transaction/fuel-transaction-dialog.html',
                    controller: 'FuelTransactionDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['FuelTransaction', function(FuelTransaction) {
                            return FuelTransaction.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })


        .state('fuel-transaction.new', {
            parent: 'fuel-transaction',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
                          views: {
                                    'content@': {
                                            templateUrl: 'app/entities/fuel-transaction/fuel-transaction-dialog.html',
                                                           controller: 'FuelTransactionDialogController',
                                                           controllerAs: 'vm',
                                                           resolve: {
                                                               entity: function () {
                                                                   return {
                                                                       driverName: null,
                                                                       vehicleNo: null,
                                                                       openingKm: null,
                                                                       closingKm: null,
                                                                       rkm: null,
                                                                       hsd: null,
                                                                       bav: null,
                                                                       dateTime: null,
                                                                       supervisorName: null,
                                                                       vehicleCompany: null,
                                                                       vehicleComingFrom: null,
                                                                       vehicleWentTo: null,
                                                                       phoneNo: null,
                                                                       hsdInwords: null,
                                                                       receiptNo: null,
                                                                       id: null
                                                                   };
                                                               }
                                                           }
                                    }
                                },

        })
        .state('fuel-transaction.edit', {
            parent: 'fuel-transaction',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/fuel-transaction/fuel-transaction-dialog.html',
                    controller: 'FuelTransactionDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['FuelTransaction', function(FuelTransaction) {
                            return FuelTransaction.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('fuel-transaction', null, { reload: 'fuel-transaction' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('fuel-transaction.delete', {
            parent: 'fuel-transaction',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/fuel-transaction/fuel-transaction-delete-dialog.html',
                    controller: 'FuelTransactionDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['FuelTransaction', function(FuelTransaction) {
                            return FuelTransaction.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('fuel-transaction', null, { reload: 'fuel-transaction' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
