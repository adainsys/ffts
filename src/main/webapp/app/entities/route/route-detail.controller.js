(function() {
    'use strict';

    angular
        .module('fftsApp')
        .controller('RouteDetailController', RouteDetailController);

    RouteDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Route'];

    function RouteDetailController($scope, $rootScope, $stateParams, previousState, entity, Route) {
        var vm = this;

        vm.route = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('fftsApp:routeUpdate', function(event, result) {
            vm.route = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
