(function() {
    'use strict';
         angular.module('fftsApp').directive('applyFileChange', applyFileChange);

          applyFileChange.$inject = ['$parse'];

          function applyFileChange($parse){
                 return {
                 restrict: 'A',
                 link: function (scope, element, attributes) {
                     element.bind('change', function () {
                         $parse(attributes.applyFileChange)
                         .assign(scope,element[0].files)
                         scope.$apply()
                     });
                 }
             };
                 }
 })();
