(function() {
    'use strict';

    angular
        .module('fftsApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
