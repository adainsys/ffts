(function() {
    'use strict';

    angular
        .module('fftsApp')
        .filter('range', range);

    function range() {
        return rangeFilter;

        function rangeFilter (input, min, max) {
             min = parseInt(min); //Make string input int
              max = parseInt(max);
              for (var i=min; i<max; i++)
                input.push(i);
              return input;
        }
    }
})();
