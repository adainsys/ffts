package com.ada.ffts.domain;


import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Objects;

/**
 * A FuelTransaction.
 */
@Entity
@Table(name = "fuel_transaction")
public class FuelTransaction implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(min = 1, max = 1024)
    @Column(name = "driver_name", length = 1024, nullable = false)
    private String driverName;

    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "vehicle_no", length = 12, nullable = false)
    private String vehicleNo;

    @NotNull
    @Size(max = 20)
    @Column(name = "opening_km", length = 20, nullable = false)
    private String openingKm;

    @NotNull
    @Size(max = 20)
    @Column(name = "closing_km", length = 20, nullable = false)
    private String closingKm;

    @Column(name = "rkm")
    private String rkm;

    @Column(name = "hsd")
    private String hsd;

    @Column(name = "bav")
    private String bav;

    @Column(name = "date_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateTime;

    @NotNull
    @Column(name = "supervisor_name", nullable = false)
    private String supervisorName;

    @NotNull
    @Column(name = "vehicle_company", nullable = false)
    private String vehicleCompany;

    @NotNull
    @Column(name = "vehicle_coming_from", nullable = false)
    private String vehicleComingFrom;

    @NotNull
    @Column(name = "vehicle_went_to", nullable = false)
    private String vehicleWentTo;

    @NotNull
    @Size(min = 10, max = 11)
    @Column(name = "phone_no", length = 11, nullable = false)
    private String phoneNo;

    @Column(name = "hsd_inwords")
    private String hsdInwords;

    @NotNull
    @Column(name = "receipt_no", nullable = false)
    private String receiptNo;

    @ManyToOne
    private Vehicle vehicle;


    @Column(name = "opening_kms_reading")
    private String openingKMsReading;

    @Column(name = "receipt")
    private String receipt;

    @Column(name = "fuel_reading")
    private String fuelReading;

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDriverName() {
        return driverName;
    }

    public FuelTransaction driverName(String driverName) {
        this.driverName = driverName;
        return this;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public FuelTransaction vehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
        return this;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getOpeningKm() {
        return openingKm;
    }

    public FuelTransaction openingKm(String openingKm) {
        this.openingKm = openingKm;
        return this;
    }

    public void setOpeningKm(String openingKm) {
        this.openingKm = openingKm;
    }

    public String getClosingKm() {
        return closingKm;
    }

    public FuelTransaction closingKm(String closingKm) {
        this.closingKm = closingKm;
        return this;
    }

    public void setClosingKm(String closingKm) {
        this.closingKm = closingKm;
    }

    public String getRkm() {
        return rkm;
    }

    public FuelTransaction rkm(String rkm) {
        this.rkm = rkm;
        return this;
    }

    public void setRkm(String rkm) {
        this.rkm = rkm;
    }

    public String getHsd() {
        return hsd;
    }

    public FuelTransaction hsd(String hsd) {
        this.hsd = hsd;
        return this;
    }

    public void setHsd(String hsd) {
        this.hsd = hsd;
    }

    public String getBav() {
        return bav;
    }

    public FuelTransaction bav(String bav) {
        this.bav = bav;
        return this;
    }

    public void setBav(String bav) {
        this.bav = bav;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public FuelTransaction dateTime(Date dateTime) {
        this.dateTime = dateTime;
        return this;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String getSupervisorName() {
        return supervisorName;
    }

    public FuelTransaction supervisorName(String supervisorName) {
        this.supervisorName = supervisorName;
        return this;
    }

    public void setSupervisorName(String supervisorName) {
        this.supervisorName = supervisorName;
    }

    public String getVehicleCompany() {
        return vehicleCompany;
    }

    public FuelTransaction vehicleCompany(String vehicleCompany) {
        this.vehicleCompany = vehicleCompany;
        return this;
    }

    public void setVehicleCompany(String vehicleCompany) {
        this.vehicleCompany = vehicleCompany;
    }

    public String getVehicleComingFrom() {
        return vehicleComingFrom;
    }

    public FuelTransaction vehicleComingFrom(String vehicleComingFrom) {
        this.vehicleComingFrom = vehicleComingFrom;
        return this;
    }

    public void setVehicleComingFrom(String vehicleComingFrom) {
        this.vehicleComingFrom = vehicleComingFrom;
    }

    public String getVehicleWentTo() {
        return vehicleWentTo;
    }

    public FuelTransaction vehicleWentTo(String vehicleWentTo) {
        this.vehicleWentTo = vehicleWentTo;
        return this;
    }

    public void setVehicleWentTo(String vehicleWentTo) {
        this.vehicleWentTo = vehicleWentTo;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public FuelTransaction phoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
        return this;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getHsdInwords() {
        return hsdInwords;
    }

    public FuelTransaction hsdInwords(String hsdInwords) {
        this.hsdInwords = hsdInwords;
        return this;
    }

    public void setHsdInwords(String hsdInwords) {
        this.hsdInwords = hsdInwords;
    }

    public String getReceiptNo() {
        return receiptNo;
    }

    public FuelTransaction receiptNo(String receiptNo) {
        this.receiptNo = receiptNo;
        return this;
    }

    public void setReceiptNo(String receiptNo) {
        this.receiptNo = receiptNo;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }


    public String getOpeningKMsReading() {
        return openingKMsReading;
    }

    public String getReceipt() {
        return receipt;
    }

    public String getFuelReading() {
        return fuelReading;
    }

    public void setOpeningKMsReading(String openingKMsReading) {
        this.openingKMsReading = openingKMsReading;
    }

    public void setReceipt(String receipt) {
        this.receipt = receipt;
    }

    public void setFuelReading(String fuelReading) {
        this.fuelReading = fuelReading;
    }

    public FuelTransaction vehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
        return this;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }
    // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FuelTransaction fuelTransaction = (FuelTransaction) o;
        if (fuelTransaction.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), fuelTransaction.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "FuelTransaction{" +
            "id=" + getId() +
            ", driverName='" + getDriverName() + "'" +
            ", vehicleNo='" + getVehicleNo() + "'" +
            ", openingKm='" + getOpeningKm() + "'" +
            ", closingKm='" + getClosingKm() + "'" +
            ", rkm='" + getRkm() + "'" +
            ", hsd='" + getHsd() + "'" +
            ", bav='" + getBav() + "'" +
            ", dateTime='" + getDateTime() + "'" +
            ", supervisorName='" + getSupervisorName() + "'" +
            ", vehicleCompany='" + getVehicleCompany() + "'" +
            ", vehicleComingFrom='" + getVehicleComingFrom() + "'" +
            ", vehicleWentTo='" + getVehicleWentTo() + "'" +
            ", phoneNo='" + getPhoneNo() + "'" +
            ", hsdInwords='" + getHsdInwords() + "'" +
            ", receiptNo='" + getReceiptNo() + "'" +
            "}";
    }
}
