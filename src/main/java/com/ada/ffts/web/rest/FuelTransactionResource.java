package com.ada.ffts.web.rest;

import com.ada.ffts.config.ApplicationProperties;
import com.codahale.metrics.annotation.Timed;
import com.ada.ffts.domain.FuelTransaction;
import com.ada.ffts.service.FuelTransactionService;
import com.ada.ffts.web.rest.util.HeaderUtil;
import com.ada.ffts.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * REST controller for managing FuelTransaction.
 */
@RestController
@RequestMapping("/api")
public class FuelTransactionResource {

    private final Logger log = LoggerFactory.getLogger(FuelTransactionResource.class);

    private static final String ENTITY_NAME = "fuelTransaction";

    private final FuelTransactionService fuelTransactionService;
    @Autowired
    private ApplicationProperties properties;

    public FuelTransactionResource(FuelTransactionService fuelTransactionService) {
        this.fuelTransactionService = fuelTransactionService;
    }

    /**
     * POST  /fuel-transactions : Create a new fuelTransaction.
     *
     * @param fuelTransaction the fuelTransaction to create
     * @return the ResponseEntity with status 201 (Created) and with body the new fuelTransaction, or with status 400 (Bad Request) if the fuelTransaction has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */


    @RequestMapping(value="/fuel-transactions/upload",method = RequestMethod.POST,consumes = "multipart/form-data")
    @Timed
    public ResponseEntity<Void> uploadFuelFile(HttpServletRequest request) throws Exception  {
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        MultipartFile multipartFile = multipartRequest.getFile("file");

        String newFileName = UUID.randomUUID().toString() + ".jpg";
        byte[] bytes = multipartFile.getBytes();

        String copyLocation="D:/angularUpload/" + newFileName;

        File file = new File(copyLocation);
        try {
            FileCopyUtils.copy(bytes, file);
        }catch (Exception e){

        }
        HttpHeaders headers = new HttpHeaders();
        headers.add("filePath",newFileName);
        return ResponseEntity.ok()
            .headers(headers).build();

    }


    @RequestMapping(value="/fuel-transactions",method = RequestMethod.POST,consumes = "multipart/form-data")
    @Timed
    public ResponseEntity<FuelTransaction> createFuelTransaction(@Valid @RequestPart(value="data") FuelTransaction fuelTransaction,@RequestPart("file") MultipartFile[] multipartFiles) throws Exception {
        log.debug("REST request to save FuelTransaction : {}", fuelTransaction);
        if (fuelTransaction.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new fuelTransaction cannot already have an ID")).body(null);
        }
        int index=0;
        for(MultipartFile multipartFile:multipartFiles) {
            String extension=multipartFile.getOriginalFilename().substring(multipartFile.getOriginalFilename().lastIndexOf("."));
            byte[] bytes = multipartFile.getBytes();
            String newFileName = UUID.randomUUID().toString() + extension;
            String copyLocation=properties.getDataFolder() + newFileName;
            File file = new File(copyLocation);
            FileCopyUtils.copy(bytes, file);
            if(index==0)
                fuelTransaction.setOpeningKMsReading(newFileName);
                else if(index==1)
                fuelTransaction.setReceipt(newFileName);
                    else
                fuelTransaction.setFuelReading(newFileName);
            index++;
        }




        FuelTransaction result = fuelTransactionService.save(fuelTransaction);
        return ResponseEntity.created(new URI("/api/fuel-transactions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /fuel-transactions : Updates an existing fuelTransaction.
     *
     * @param fuelTransaction the fuelTransaction to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated fuelTransaction,
     * or with status 400 (Bad Request) if the fuelTransaction is not valid,
     * or with status 500 (Internal Server Error) if the fuelTransaction couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    /*@PutMapping("/fuel-transactions")
    @Timed
    public ResponseEntity<FuelTransaction> updateFuelTransaction(@Valid @RequestBody FuelTransaction fuelTransaction,HttpServletRequest request) throws Exception {
        log.debug("REST request to update FuelTransaction : {}", fuelTransaction);
        if (fuelTransaction.getId() == null) {
            return createFuelTransaction(fuelTransaction,request);
        }
        FuelTransaction result = fuelTransactionService.save(fuelTransaction);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, fuelTransaction.getId().toString()))
            .body(result);
    }*/

    /**
     * GET  /fuel-transactions : get all the fuelTransactions.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of fuelTransactions in body
     */
    @GetMapping("/fuel-transactions")
    @Timed
    public ResponseEntity<List<FuelTransaction>> getAllFuelTransactions(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of FuelTransactions");
        Page<FuelTransaction> page = fuelTransactionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/fuel-transactions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /fuel-transactions/:id : get the "id" fuelTransaction.
     *
     * @param id the id of the fuelTransaction to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the fuelTransaction, or with status 404 (Not Found)
     */
    @GetMapping("/fuel-transactions/{id}")
    @Timed
    public ResponseEntity<FuelTransaction> getFuelTransaction(@PathVariable Long id) {
        log.debug("REST request to get FuelTransaction : {}", id);
        FuelTransaction fuelTransaction = fuelTransactionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(fuelTransaction));
    }

    /**
     * DELETE  /fuel-transactions/:id : delete the "id" fuelTransaction.
     *
     * @param id the id of the fuelTransaction to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/fuel-transactions/{id}")
    @Timed
    public ResponseEntity<Void> deleteFuelTransaction(@PathVariable Long id) {
        log.debug("REST request to delete FuelTransaction : {}", id);
        fuelTransactionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
