/**
 * View Models used by Spring MVC REST controllers.
 */
package com.ada.ffts.web.rest.vm;
