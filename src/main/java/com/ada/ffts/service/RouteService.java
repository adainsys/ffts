package com.ada.ffts.service;

import com.ada.ffts.domain.Route;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Route.
 */
public interface RouteService {

    /**
     * Save a route.
     *
     * @param route the entity to save
     * @return the persisted entity
     */
    Route save(Route route);

    /**
     *  Get all the routes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<Route> findAll(Pageable pageable);

    /**
     *  Get the "id" route.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Route findOne(Long id);

    /**
     *  Delete the "id" route.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
