package com.ada.ffts.service;

import com.ada.ffts.domain.FuelTransaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing FuelTransaction.
 */
public interface FuelTransactionService {

    /**
     * Save a fuelTransaction.
     *
     * @param fuelTransaction the entity to save
     * @return the persisted entity
     */
    FuelTransaction save(FuelTransaction fuelTransaction);

    /**
     *  Get all the fuelTransactions.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<FuelTransaction> findAll(Pageable pageable);

    /**
     *  Get the "id" fuelTransaction.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    FuelTransaction findOne(Long id);

    /**
     *  Delete the "id" fuelTransaction.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
