package com.ada.ffts.service.impl;

import com.ada.ffts.service.RouteService;
import com.ada.ffts.domain.Route;
import com.ada.ffts.repository.RouteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing Route.
 */
@Service
@Transactional
public class RouteServiceImpl implements RouteService{

    private final Logger log = LoggerFactory.getLogger(RouteServiceImpl.class);

    private final RouteRepository routeRepository;
    public RouteServiceImpl(RouteRepository routeRepository) {
        this.routeRepository = routeRepository;
    }

    /**
     * Save a route.
     *
     * @param route the entity to save
     * @return the persisted entity
     */
    @Override
    public Route save(Route route) {
        log.debug("Request to save Route : {}", route);
        return routeRepository.save(route);
    }

    /**
     *  Get all the routes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Route> findAll(Pageable pageable) {
        log.debug("Request to get all Routes");
        return routeRepository.findAll(pageable);
    }

    /**
     *  Get one route by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Route findOne(Long id) {
        log.debug("Request to get Route : {}", id);
        return routeRepository.findOne(id);
    }

    /**
     *  Delete the  route by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Route : {}", id);
        routeRepository.delete(id);
    }
}
