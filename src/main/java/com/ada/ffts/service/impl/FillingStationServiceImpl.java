package com.ada.ffts.service.impl;

import com.ada.ffts.service.FillingStationService;
import com.ada.ffts.domain.FillingStation;
import com.ada.ffts.repository.FillingStationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing FillingStation.
 */
@Service
@Transactional
public class FillingStationServiceImpl implements FillingStationService{

    private final Logger log = LoggerFactory.getLogger(FillingStationServiceImpl.class);

    private final FillingStationRepository fillingStationRepository;
    public FillingStationServiceImpl(FillingStationRepository fillingStationRepository) {
        this.fillingStationRepository = fillingStationRepository;
    }

    /**
     * Save a fillingStation.
     *
     * @param fillingStation the entity to save
     * @return the persisted entity
     */
    @Override
    public FillingStation save(FillingStation fillingStation) {
        log.debug("Request to save FillingStation : {}", fillingStation);
        return fillingStationRepository.save(fillingStation);
    }

    /**
     *  Get all the fillingStations.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<FillingStation> findAll(Pageable pageable) {
        log.debug("Request to get all FillingStations");
        return fillingStationRepository.findAll(pageable);
    }

    /**
     *  Get one fillingStation by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public FillingStation findOne(Long id) {
        log.debug("Request to get FillingStation : {}", id);
        return fillingStationRepository.findOne(id);
    }

    /**
     *  Delete the  fillingStation by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete FillingStation : {}", id);
        fillingStationRepository.delete(id);
    }
}
