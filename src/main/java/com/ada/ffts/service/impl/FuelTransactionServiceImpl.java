package com.ada.ffts.service.impl;

import com.ada.ffts.service.FuelTransactionService;
import com.ada.ffts.domain.FuelTransaction;
import com.ada.ffts.repository.FuelTransactionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing FuelTransaction.
 */
@Service
@Transactional
public class FuelTransactionServiceImpl implements FuelTransactionService{

    private final Logger log = LoggerFactory.getLogger(FuelTransactionServiceImpl.class);

    private final FuelTransactionRepository fuelTransactionRepository;
    public FuelTransactionServiceImpl(FuelTransactionRepository fuelTransactionRepository) {
        this.fuelTransactionRepository = fuelTransactionRepository;
    }

    /**
     * Save a fuelTransaction.
     *
     * @param fuelTransaction the entity to save
     * @return the persisted entity
     */
    @Override
    public FuelTransaction save(FuelTransaction fuelTransaction) {
        log.debug("Request to save FuelTransaction : {}", fuelTransaction);
        return fuelTransactionRepository.save(fuelTransaction);
    }

    /**
     *  Get all the fuelTransactions.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<FuelTransaction> findAll(Pageable pageable) {
        log.debug("Request to get all FuelTransactions");
        return fuelTransactionRepository.findAll(pageable);
    }

    /**
     *  Get one fuelTransaction by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public FuelTransaction findOne(Long id) {
        log.debug("Request to get FuelTransaction : {}", id);
        return fuelTransactionRepository.findOne(id);
    }

    /**
     *  Delete the  fuelTransaction by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete FuelTransaction : {}", id);
        fuelTransactionRepository.delete(id);
    }
}
