package com.ada.ffts.service;

import com.ada.ffts.domain.FillingStation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing FillingStation.
 */
public interface FillingStationService {

    /**
     * Save a fillingStation.
     *
     * @param fillingStation the entity to save
     * @return the persisted entity
     */
    FillingStation save(FillingStation fillingStation);

    /**
     *  Get all the fillingStations.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<FillingStation> findAll(Pageable pageable);

    /**
     *  Get the "id" fillingStation.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    FillingStation findOne(Long id);

    /**
     *  Delete the "id" fillingStation.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
