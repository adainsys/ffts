package com.ada.ffts.repository;

import com.ada.ffts.domain.FuelTransaction;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the FuelTransaction entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FuelTransactionRepository extends JpaRepository<FuelTransaction, Long> {

}
