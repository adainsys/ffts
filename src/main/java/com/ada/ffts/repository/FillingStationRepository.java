package com.ada.ffts.repository;

import com.ada.ffts.domain.FillingStation;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the FillingStation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FillingStationRepository extends JpaRepository<FillingStation, Long> {

}
